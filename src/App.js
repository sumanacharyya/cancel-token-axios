import "./App.css";
import axios from "axios";

function App() {
  const onChangeHAndler = async (e) => {
    const val = e.target.value;
    let cancelToken;

    //? DEBOUNCE_ON_CHANGE

    if (typeof cancelToken != typeof undefined) {
      cancelToken.cancel("Previous request cancelled");
    }
    cancelToken = axios.CancelToken.source();

    const res = await axios.get(`http://localhost:8080/users?q=${val}`, {
      cancelToken: cancelToken.token,
    });

    console.table(res);
  };

  return (
    <div className="App">
      <input
        type="text"
        placeholder="Enter search keyword"
        onChange={onChangeHAndler}
      />
    </div>
  );
}

export default App;
